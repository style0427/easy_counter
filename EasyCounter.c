#include <stdio.h>
#include <math.h>
int main(void)
{
 printf("**************************************\n");
 printf("###########冻梨牌简易计算器###########\n");
 void Menu_f();/* 主菜单*/
 void Help();/* 帮助菜单*/
 void SolveX_f(); /* 求解一元二次方程解 */
 void MmToM_f(); /* mm与m单位换算 */
 void ScanMax_f(); /* 查找10个数的最大值，及其所在的位置 */
 void ScanMin_f(); /* 查找10个数的最小值，及其所在的位置 */
 void MaMi();/* 查找10个数的最大/小值的函数 */
 void AboutDouble_f(); /* 求解两个数的最大公约数和最小公倍数 */
 void SqR_f();/* 求解平方根运算 */
 void FourCount_f();/* 四则运算 */
 void add(); /* 加法运算 */
 void dec(); /* 减法运算 */
 void chen(); /*乘法运算 */
 void chu();/* 除法运算 */
 void MaxY();/* 求最大公约数运算 */
 void MinB();/* 求最小公倍数运算 */
 int n;
 do
 {
 Menu_f(); 
 scanf("%d", &n);
 switch(n)
 {
 case 1:
 Help(); /* 帮助菜单*/
 break;
 case 2:
 SolveX_f(); /* 求解一元二次方程解 */
 break;
 case 3:
 MmToM_f(); /* mm与m */
 break;
 case 4:
 MaMi(); /* 查找10个数的最大/小值，及其所在的位置 */
 break;
 case 5:
 AboutDouble_f(); /* 求解两个数的最大公约数和最小公倍数 */
 break;
 case 6:
 SqR_f();/* 求解平方根运算 */
 break;
 case 7:
 FourCount_f();/* 加、减、乘、除运算 */
 break;
 case 8:/* 退出 */
 printf("谢谢使用！\n");
 return 0;
 default:
 printf("请输入以上正确的序号\n");
 break;
 }
 }while(n!=8);/* 防止输入错误数字导致直接退出程序 */
 return 0;
}

void Menu_f()
{
 printf("*****请输入以下正确的序号进行程序*****\n");
 printf("######################################\n");
 printf("1.帮助菜单\n");
 printf("2.求解一元二次方程的解\n");
 printf("3.将mm与m单位换算\n");
 printf("4.查找10个数的最大/小值，及其所在的位置\n");
 printf("5.求解两个数的最大公约数或最小公倍数\n");
 printf("6.求解平方根运算\n");
 printf("7.加、减、乘、除运算\n");
 printf("8.退出\n");
 printf("######################################\n");
 printf("**************************************\n");
}

void Help()
{
	 printf("**************************************\n");
     printf("#############程序序号如下#############\n");
     printf("1.帮助菜单\n");
     printf("2.求解一元二次方程的解\n");
     printf("3.将mm与m单位换算\n");
     printf("4.查找10个数的最大/小值，及其所在的位置\n");
     printf("5.求解两个数的最大公约数或最小公倍数\n");
     printf("6.求解平方根运算\n");
     printf("7.加、减、乘、除运算\n");
     printf("8.退出\n");
     printf("#######################################\n");
     printf("**************************************\n");
}

void SolveX_f()
{
    float a,b,c,x1,x2,d;
    printf("请输入x2的系数:");
    scanf("%f",&a);
    printf("请输入x的系数:");
    scanf("%f",&b);
    printf("请输入常数项:");
    scanf("%f",&c);
	printf("方程为:%.0fx*x+%.0fx+%.0f=0\n",a,b,c);
    d=b*b-4*a*c;
	if(d<0)
	{
     printf("方程没有实数解\n");
	}
    if(d==0)
	{
	 x1=(-b)/(2*a);
     printf("函数有一解，为:x1=%f\n",x1);
	}
    if(d>0)
	{
	 x1=(-b+sqrt(d))/(2*a);
     x2=(-b-sqrt(d))/(2*a);
     printf("函数有两解，分别为:x1=%f,x2=%f\n",x1,x2);
	}
}

void MmToM_f()
{
	int num;
	do
	{
	printf("******************************\n");
    printf("$$$$请输入你需要的运算序号$$$$\n");
	printf("##############################\n");
    printf("1.mm转换成m\n");
	printf("2.m转换成mm\n");
	printf("3.返回主菜单\n");
    printf("##############################\n");
	printf("******************************\n");
	float m;
	scanf("%d", &num);
	switch(num)
	{
	case 1:
	printf("请输入数字:\n");
	scanf("%f",&m);
	m=0.01*m;
	printf("结果为:%f\n",m);
	break;
	case 2:
	printf("请输入数字:\n");
	scanf("%f",&m);
	m=100*m;
	printf("结果为:%f\n",m);
	break;
	case 3:
	break;
	default:
    printf("请输入以上正确的序号\n");
    break;
	}
	}while(num!=3);/* 防止输入错误数字导致直接退出程序 */
}

void AboutDouble_f()
{
	int num;
	do
	{
	printf("******************************\n");
    printf("$$$$请输入你需要的运算序号$$$$\n");
	printf("##############################\n");
    printf("1.最大公约数\n");
	printf("2.最小公倍数\n");
	printf("3.返回主菜单\n");
	printf("##############################\n");
	printf("******************************\n");
	scanf("%d", &num);
	switch(num)
	{
	case 1:
	void MaxY();
	MaxY();
	break;
	case 2:
	void MinB();
	MinB();
	break;
	case 3:
	break;
	default:
    printf("请输入以上正确的序号\n");
    break;
	}
	}while(num!=3);/* 防止输入错误数字导致直接退出程序 */
}

void FourCount_f()
{	
	int num;
	do
	{
	void add(); 
    void dec(); 
    void chen(); 
    void chu();
	printf("******************************\n");
	printf("****请输入你需要的运算序号****\n");
	printf("##############################\n");
    printf("1.加法\n");
	printf("2.减法\n");
	printf("3.乘法\n");
	printf("4.除法\n");
	printf("5.返回主菜单\n");
	printf("##############################\n");
	printf("******************************\n");
	scanf("%d", &num);
	switch(num)
	{
	case 1:
	add();
	break;
	case 2:
	dec();
	break;
	case 3:
	chen();
	break;
	case 4:
	chu();
	break;
	case 5:
	break;
	default:
    printf("请输入以上正确的序号\n");
    break;
	}
	}while(num!=5);/* 防止输入错误数字导致直接退出程序 */
}

void SqR_f()
{
	float m;
	printf("请输入:\n");
	scanf("%f",&m);
	m=sqrt(m);
	printf("结果为:%f\n",m);
}

void add()
{
	int a,b,s;
	printf("请输入整数:\n");
	printf("第一个数:");
	scanf("%d",&a);
	printf("第二个数:");
	scanf("%d",&b);
	s=a+b;
	printf("结果为:和=%d\n",s);
}

void dec()
{
	int a,b,d;
	printf("请输入整数:\n");
	printf("第一个数:\n");
	scanf("%d",&a);
	printf("第二个数:\n");
	scanf("%d",&b);
	d=a-b;
	printf("结果为:差=%d\n",d);
}

void chen()
{
	int a,b,s;
	printf("请输入整数:\n");
	printf("第一个数:\n");
	scanf("%d",&a);
	printf("第二个数:\n");
	scanf("%d",&b);
	s=a*b;
	printf("结果为:积=%d\n",s);
}

void chu()
{
	int a,b,s;
	printf("请输入整数:\n");
	printf("第一个数:\n");
	scanf("%d",&a);
	printf("第二个数:\n");
	scanf("%d",&b);
	s=a+b;
	printf("结果为:商=%d\n",s);
}

void MaxY()
{
	int a,b,c,i,j=1;
	printf("请输入:\n");
	printf("第一个数:");
	scanf("%d",&a);
	printf("第二个数:");
	scanf("%d",&b);
	if(a>b)
	{
		c=a;a=b;b=c;
	}
	for(i=a;i>0;i--)
	{
	  if(a%i==0&&b%i==0)
	  {
		j=i;
		printf("最大公因数是:%d\n",j);
	   break;
	  }
	}
}

void MinB()
{
	int a,b,c,i,j=1,m;
	printf("请输入:\n");
	printf("第一个数:");
	scanf("%d",&a);
	printf("第二个数:");
	scanf("%d",&b);
	if(a>b)
	{
		c=a;a=b;b=c;
	}
	for(i=a;i>0;i--)
	{
	  if(a%i==0&&b%i==0)
		j=i;
	    break;
	}
	m=(a/j)*b; /*用最大公因数求最小公倍数*/
	printf("最小公倍数是:%d\n",m);
}

void ScanMax_f()
{
  int a[10],i,j,max;
  printf("请输入10个数字:\n");
  for(i=0;i<10;i++)
    scanf("%d,",&a[i]);
  	max=a[0];
  for(i=0;i<10;i++)
  {
	if(max<a[i])
	{
		max=a[i];
		j=i+1;
	}
  }
  printf("最大数为:%d,",max);
  printf("为数字%d\n",j);
}

void ScanMin_f()
{
  int a[10],i,j,min;
  printf("请输入10个数字:\n");
  for(i=0;i<10;i++)
    scanf("%d,",&a[i]);
  	min=a[0];
  for(i=0;i<10;i++)
  {
	if(min>a[i])
	{
		min=a[i];
		j=i+1;
	}
  }
  printf("最小数为:%d,",min);
  printf("为数字%d\n",j);
}

void MaMi()
{
	int num;
	do
	{
	 printf("******************************\n");
     printf("$$$$请输入你需要的运算序号$$$$\n");
	 printf("##############################\n");
     printf("1.求最大值\n");
	 printf("2.求最小值\n");
	 printf("3.返回主菜单\n");
	 printf("##############################\n");
	 printf("******************************\n");
	 scanf("%d", &num);
	 switch(num)
	 {
	 case 1:
	 ScanMax_f();
	 break;
	 case 2:
	 ScanMin_f();
	 break;
	 case 3:
	 break;
	 default:
     printf("请输入以上正确的序号\n");
     break;
	 }
	}while(num!=3);/* 防止输入错误数字导致直接退出程序 */
}